## enable driver:

```
sudo echo "dtoverlay=dwc2" >> /boot/config.txt
sudo echo "dwc2" >> /etc/modules
sudo reboot
```
## create storage device:

```
sudo /sbin/modprobe g_mass_storage file=recipes.img stall=0 ro=y cdrom=y iSerialNumber="your_serial_number"
```
