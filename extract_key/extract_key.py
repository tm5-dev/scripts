import hashlib
from subprocess import check_output
#kernel memory dump file
KERNEL_DUMP='kernel.bin'
CMD_HEXDUMP = 'hexdump -C '+KERNEL_DUMP
ikaros_key = 'place_here'
#hash key, take first 16 bytes
cmp_key = hashlib.sha256(ikaros_key.encode()).hexdigest()[:32]
space_key = ''
key_counter = 0
#add whitespaces to search in hexdump
while(len(cmp_key)+15 > len(space_key)):
	space_key+=cmp_key[key_counter]+cmp_key[key_counter+1]+' '
	key_counter= key_counter+2
	if key_counter == 8:
		space_key +=' '
#save and remove last 4 bytes
space_remaining = space_key[len(space_key)-12]+space_key[len(space_key)-11]
space_remaining+= space_key.split(space_remaining)[1]
space_key = space_key[:-12]
#get the next two lines after the key
out = str(check_output(CMD_HEXDUMP.split(' '))).split('|')
for idx, line in enumerate(out):
	if space_key in line:
		break
if idx+1 < len(out):
	#format
	first_line = out[idx+2].split('  ')
	second_line = out[idx+4].split('  ')
	temp_key = first_line[1]+first_line[2]+second_line[1]+second_line[2]
	#remove remainings from known key
	temp_key = temp_key.split(space_remaining)[1]
	#remove zeros
	temp_key = temp_key.split('00 00')[0]
	#remove whitespaces, print key
	print(temp_key.replace(' ',''))
else:
	print('Can not find key in kernel dump. Please check ikaros key and the uncompressed kernel dump!')
