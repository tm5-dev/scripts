# emulation
emulate firmware without a gui
- place `1.squashfs` and `tm5.img` into the same directory
- place tools into `tools`
- `sudo bash tm5.sh`
- kernel and initrd will be downloaded
- login: root, password: root
- `tm5_chroot`
## unpack update
- `cd /opt`
- `checkimg -d tm5.img [data/extra/firmware].pack`