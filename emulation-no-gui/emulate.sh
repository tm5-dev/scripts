#!/bin/sh
#based on https://www.mikrocontroller.net/attachment/347570/tm5.sh by Joachim S.(oyo)
# Function for printing colorized text to STDOUT
message() {
	text="$1"
	escape_code=${2:-"1;32"}
	printf "\033[0;%sm%s\033[0m\n" "${escape_code}" "${text}"
}
sudo modprobe nbd max_part=16
# Ensure that a kernel file named "vmlinuz" exists
if [ ! -f vmlinuz ]; then
	# If it does not exist, download one and create a symlink
	message "Downloading kernel file vmlinuz..."
	wget https://people.debian.org/~aurel32/qemu/armel/vmlinuz-2.6.32-5-versatile
	ln -s vmlinuz-2.6.32-5-versatile vmlinuz
fi

# Ensure that a initrd file named "initrd.img" exists
if [ ! -f initrd.img ]; then
	# If it does not exist, download one and create a symlink
	message "Downloading initial ramdisk file initrd.img..."
	wget https://people.debian.org/~aurel32/qemu/armel/initrd.img-2.6.32-5-versatile
	ln -s initrd.img-2.6.32-5-versatile initrd.img
fi

# Ensure that a qcow2 QEMU image file named "disk.qcow2" exists
if [ ! -f disk.qcow2 ]; then
	# If it does not exit, create it as a simple copy of file "disk.initial.qcow2"
	# Ensure that a qcow2 QEMU image file named "disk.initial.qcow2" exists
	# This file contains the bootable disk image, including TM5 related stuff
	if [ ! -f disk.initial.qcow2 ]; then
		# If it does not exit, create it
		# Ensure that a qcow2 QEMU image file named "disk.base.qcow2" exists
		# This file contains the basic bootable disk image, without any TM5 related stuff, with the rootfs in the first partition
		if [ ! -f disk.base.qcow2 ]; then
			# If it does not exist, download one and create a symlink
			message "Downloading base QEMU image file disk.base.qcow2..."
			wget https://people.debian.org/~aurel32/qemu/armel/debian_squeeze_armel_standard.qcow2
			ln -s debian_squeeze_armel_standard.qcow2 disk.base.qcow2
		fi
		# Ensure that a squashfs image file named "1.squashfs" exists
		if [ ! -f 1.squashfs ]; then
			# If it does not exist, download one and create a symlink
			message "ERROR: TM5 squashfs firmware file does not exist! Please download and place it in this directory as a file named 1.squashfs" "1;31"
			exit 1
		fi
		message "Creating initial QEMU image file disk.initial.qcow2..."
		# Copy file "disk.base.qcow2" to file "disk.initial.qcow2"
		cp disk.base.qcow2 disk.initial.qcow2
		# Create virtual mass storage device "/dev/nbd0" from "disk.initial.qcow2"
		sudo qemu-nbd -c /dev/nbd0 disk.initial.qcow2
		# Scan partitions
		sudo partprobe /dev/nbd0
		# Create a temporary mountpoint directory
		mkdir mountpoint
		# Mount the first partition (which contains the rootfs) into the mountpoint directory
		sudo mount /dev/nbd0p1 mountpoint
		# Extract the files in "1.squashfs" to directory "/tm5"
		sudo unsquashfs -d mountpoint/tm5 -f 1.squashfs
		# /bin/busybox in the squashfs file seems broken?!? Workaround:
		sudo cp mountpoint/bin/busybox mountpoint/tm5/bin/busybox
		# copy update img
		sudo cp tm5.img mountpoint/tm5/opt/
		# Create "tm5_chroot" command that chroots into the tm5 filesystem
		printf '#!/bin/sh\n\necho "Enter \"exit\" to exit the chroot"\nchroot /tm5 /bin/sh -i -s \n' | sudo tee mountpoint/usr/local/bin/tm5_chroot
		sudo chmod 755 mountpoint/usr/local/bin/tm5_chroot
		# Unmount the QEMU image
		sudo umount mountpoint
		# Remove virtual mass storage device "/dev/nbd0"
		sudo qemu-nbd -d /dev/nbd0
		# Remove the temporary mountpoint directory
		rmdir mountpoint
	fi
	# Then copy file "disk.initial.qcow2" to file "disk.qcow2"
	message "Creating actual QEMU image file disk.qcow2..."
	cp disk.initial.qcow2 disk.qcow2
fi

# Start the emulated system
message "======================================="
message "login with root:root, use cmds:"
message "ssh -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null root@localhost -p 8022"
message "sshfs -o UserKnownHostsFile=/dev/null root@localhost:/  your_mountpoint -p 8022"
message "after login, use tm5_chroot"
qemu-system-arm -M versatilepb -kernel vmlinuz -initrd initrd.img -hda disk.qcow2 -append "root=/dev/sda1" -nographic -nic user,hostfwd=tcp::8022-:22
